

// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // print out the value you read:
  int brightness = map(sensorValue, 0, 1024, 1024, 0);
    Serial.print(sensorValue);  
    Serial.print(" ");  
    Serial.println(brightness);

  analogWrite(LED_BUILTIN, sensorValue);
  delay(100);        // delay in between reads for stability
}
